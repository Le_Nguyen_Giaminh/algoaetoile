import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Board implements Comparable<Board> {
    private int[][] board;
    private int[][] goalBoard;
    private int size;
    private int pointer_x, pointer_y;
    private Board pred;

    public Board(int n, int[][] b) {
        board = b;
        setPointer();
        goalBoard = new int[n][n];
        size = n;
        int cpt = 1;
        for(int i = 0; i<n; i++) {
            for(int j = 0; j<n; j++){
                if(i==(n-1) && j==(n-1)){
                    goalBoard[i][j] = 0;
                }
                else {
                    goalBoard[i][j] = cpt;
                    cpt++;
                }
            }
        }
    }

    public int compareTo(Board b){
        if(Arrays.deepEquals(board, b.getBoard())) {
            return 1;
        }
        else {
            return 0;
        }
    }

    public Board getGoalBoard() {
        return new Board(size, goalBoard);
    }

    public void setPointer() {
        for(int i = 0; i<size; i++) {
            for(int j = 0; j<size; j++){
                if(board[i][j]==0){
                    pointer_x = i;
                    pointer_y = j;
                }
            }
        }
    }

    public void setPred(Board b) {
        pred = b;
    }

    public void setCase(int x, int y, int val) {
        board[x][y] = val;
    }

    public String toString() {
        String res = "";
        for(int i=0; i<board.length; i++) {
            for(int j=0; j<board.length; j++) {
                res+=board[i][j] + " ";
            }
            res+="\n";
        }
        return res;
    }

    public int getSize(){
        return size;
    }

    public int getCase(int i, int j) {
        return board[i][j];
    }

    public int[][] getBoard() {
        return board;
    }

    public Board getPred() {
        return pred;
    }

    public boolean isGoalBoard() {
        for(int i=0; i<board.length; i++){
            for(int j=0; j<board.length; j++) {
                if(board[i][j]!=goalBoard[i][j]) {
                    return false;
                }
            }
        }
        return true;
    }

    public int getHeuristic() {
        int cpt = 0;
        for(int i=0; i<board.length; i++){
            for(int j=0; j<board.length; j++) {
                if(board[i][j]==goalBoard[i][j]) {
                    cpt++;
                }
            }
        }
        return cpt;
    }

    public List<String> getNeighborsOfPointer() {
        List<String> res = new ArrayList<>();
        if(pointer_x > 0) {res.add("L");}; //left
        if(pointer_y > 0) {res.add("A");}; //above
        if(pointer_x < (size-1)) {res.add("R");}; //right
        if(pointer_y < (size-1)) {res.add("U");}; //under
        return res;
    }

    public Map<Board,Integer> generateNextBoard() {
        Map<Board, Integer> nextBoards = new HashMap<>();
        List<String> neighbors = this.getNeighborsOfPointer();

        for(int i=0; i<neighbors.size(); i++) {
            if(neighbors.get(i).equals("L")) {
                Board newBoard = new Board(this.size, this.getBoard());
                newBoard.setCase(pointer_x, pointer_y, newBoard.getBoard()[pointer_x-1][pointer_y]);
                newBoard.setCase(pointer_x-1, pointer_y, 0);
                newBoard.setPointer();
                newBoard.setPred(this);
                nextBoards.put(newBoard, newBoard.getHeuristic());
            }
            
            if(neighbors.get(i).equals("A")) {
                Board newBoard = new Board(this.size, this.getBoard());
                newBoard.setCase(pointer_x, pointer_y, newBoard.getBoard()[pointer_x][pointer_y-1]);
                newBoard.setCase(pointer_x, pointer_y-1, 0);
                newBoard.setPointer();
                newBoard.setPred(this);
                nextBoards.put(newBoard, newBoard.getHeuristic());
            }

            if(neighbors.get(i).equals("R")) {
                Board newBoard = new Board(this.size, this.getBoard());
                newBoard.setCase(pointer_x, pointer_y, newBoard.getBoard()[pointer_x+1][pointer_y]);
                newBoard.setCase(pointer_x+1, pointer_y, 0);
                newBoard.setPointer();
                newBoard.setPred(this);
                nextBoards.put(newBoard, newBoard.getHeuristic());
            }

            if(neighbors.get(i).equals("U")) {
                Board newBoard = new Board(this.size, this.getBoard());
                newBoard.setCase(pointer_x, pointer_y, newBoard.getBoard()[pointer_x][pointer_y+1]);
                newBoard.setCase(pointer_x, pointer_y+1, 0);
                newBoard.setPointer();
                newBoard.setPred(this);
                nextBoards.put(newBoard, newBoard.getHeuristic());
            }
        }
        return nextBoards;
    }
}
