import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Set;

public class AStar {
    private PriorityQueue<Board> openNodeList;
    private Set<Board> closedNodeList;
    private int current_cost;

    public AStar(){
        openNodeList = new PriorityQueue<>();
        closedNodeList = new HashSet<Board>();
        current_cost = 100000;
    }

    public void aStarSearch(Board b) {
        openNodeList.add(b);
        Board current;

        while(true) {
            if(openNodeList.size()==0) {
                throw new RuntimeException("Impossible to find a solution");
            }
            else {
                current  = openNodeList.poll();
            }

            if(current.isGoalBoard()) {
                break;
            }

            Map<Board, Integer> nextBoards = current.generateNextBoard();
            closedNodeList.add(current);

            for(Board i : nextBoards.keySet()) {
                if(!openNodeList.contains(i) && !closedNodeList.contains(i)) {
                    int new_cost = current_cost + nextBoards.get(i);
                    if(new_cost < current_cost) {
                        current_cost = new_cost;
                        openNodeList.add(i);
                    }
                }
            }
        }

        List<Board> nodes = new ArrayList<Board>();
        Board g = b.getGoalBoard();
        g.setPred(current);
        while (g.getPred()!=null) {
            nodes.add(g.getPred());
            g = g.getPred();
        }
        nodes.add(b);
        for(int j=nodes.size(); j>=0; j--) {
            System.out.println(nodes.get(j).toString());
        }
    }
}
